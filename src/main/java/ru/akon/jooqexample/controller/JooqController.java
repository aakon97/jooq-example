package ru.akon.jooqexample.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.akon.jooqexample.service.JooqService;

@RequiredArgsConstructor
@RestController
@RequestMapping("jooq")
@Tag(description = "jooq", name = "jooq")
public class JooqController {

    private final JooqService jooqService;

    @Operation(description = "Выборка")
    @GetMapping("select")
    public String select() {
        return jooqService.select();
    }

}
