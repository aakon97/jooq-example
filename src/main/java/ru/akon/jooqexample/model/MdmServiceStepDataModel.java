package ru.akon.jooqexample.model;

import lombok.Getter;
import lombok.Setter;
import org.jooq.types.YearToSecond;

import java.time.LocalDateTime;

@Getter
@Setter
public class MdmServiceStepDataModel {
    String code;
    Long id;
    Long mfcBranchId;
    LocalDateTime serviceProcessingStartedTimestamp;
    java.time.LocalDateTime serviceProcessingEndedTimestamp;
    YearToSecond dur;
    LocalDateTime serviceProcessingHoldTimestamp;
    LocalDateTime serviceProcessingUnholdTimestamp;
    Integer regionId;
    String frguSId;
    Long idService_797;
    Long service_797Type;
    Integer idServiceProcessingEndedCause;
    Integer idServiceHoldReason;
    LocalDateTime updateDate;
}
