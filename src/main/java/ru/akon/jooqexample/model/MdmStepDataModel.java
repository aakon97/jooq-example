package ru.akon.jooqexample.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class MdmStepDataModel {
    Long id;
    String code;
    String status;
    LocalDateTime updateDate;
    Integer regionId;
    String frguSId;
    String endCause;

    List<MdmServiceStepDataModel> services;
}
