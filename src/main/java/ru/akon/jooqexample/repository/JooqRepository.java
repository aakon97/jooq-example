package ru.akon.jooqexample.repository;

import ru.akon.jooqexample.model.MdmServiceStepDataModel;
import ru.akon.jooqexample.model.MdmStepDataModel;

import java.util.List;

public interface JooqRepository {
    List<MdmStepDataModel> select(Long id);

    List<MdmStepDataModel> join(Long id);
    List<MdmStepDataModel> join1(Long id);

    List<MdmServiceStepDataModel> selectService(Long id);

    MdmStepDataModel save(MdmStepDataModel model);
    MdmStepDataModel update(MdmStepDataModel model);
    Boolean delete(Long id);
}
