package ru.akon.jooqexample.repository;

import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.springframework.stereotype.Repository;
import ru.akon.jooqexample.domain.tables.MdmServiceStepData;
import ru.akon.jooqexample.domain.tables.MdmStepData;
import ru.akon.jooqexample.model.MdmServiceStepDataModel;
import ru.akon.jooqexample.model.MdmStepDataModel;

import java.util.List;

@RequiredArgsConstructor
@Repository
public class JooqRepositoryImpl implements JooqRepository{
    private final DSLContext dsl;

    public List<MdmStepDataModel> select(Long id){
        return dsl.selectFrom(MdmStepData.MDM_STEP_DATA) //select * from MDM_SERVICE_STEP_DATA
                .where(MdmStepData.MDM_STEP_DATA.ID.eq(id))  //where id = ? 164124174
                .fetch()  //здесь определяем, что мы хотим вернуть
                .into(MdmStepDataModel.class);
    }

    public List<MdmStepDataModel> join(Long id){
        return dsl.selectFrom(MdmStepData.MDM_STEP_DATA) //select * from MDM_SERVICE_STEP_DATA
                .where(MdmStepData.MDM_STEP_DATA.ID.eq(id))  //where id = ? 164124174
                .fetch()  //здесь определяем, что мы хотим вернуть
                .map(r -> {
                    MdmStepDataModel country = r.into(MdmStepDataModel.class);
                    country.setServices(selectService(country.getId()));
                    return country;
                });
    }

    public List<MdmStepDataModel> join1(Long id){
        return dsl.select() //select * from MDM_SERVICE_STEP_DATA
                .from(MdmStepData.MDM_STEP_DATA)
                .join(MdmServiceStepData.MDM_SERVICE_STEP_DATA)
                .on(MdmStepData.MDM_STEP_DATA.ID.eq(MdmServiceStepData.MDM_SERVICE_STEP_DATA.ID))
                .where(MdmStepData.MDM_STEP_DATA.ID.eq(id))
                .fetch()  //здесь определяем, что мы хотим вернуть
                .into(MdmStepDataModel.class);
    }

    public List<MdmServiceStepDataModel> selectService(Long id){
        return dsl.selectFrom(MdmServiceStepData.MDM_SERVICE_STEP_DATA) //select * from MDM_SERVICE_STEP_DATA
                .where(MdmServiceStepData.MDM_SERVICE_STEP_DATA.ID.eq(id))  //where id = ? 164124174
                .fetch()  //здесь определяем, что мы хотим вернуть
                .into(MdmServiceStepDataModel.class);
    }

    @Override
    public MdmStepDataModel save(MdmStepDataModel model) {
        return dsl.insertInto(MdmStepData.MDM_STEP_DATA)  //insert into MDM_SERVICE_STEP_DATA
                .values(model.getId(), model.getCode(), model.getStatus(), model.getUpdateDate(), model.getRegionId(), model.getFrguSId(), model.getEndCause())  //values (? ? ? ?)
                .returning()  //returning
                .fetchOne()  //*
                .into(MdmStepDataModel.class);
    }

    @Override
    public MdmStepDataModel update(MdmStepDataModel model) {
        return dsl.update(MdmStepData.MDM_STEP_DATA)
                .set(dsl.newRecord(MdmStepData.MDM_STEP_DATA, model))
                .where(MdmStepData.MDM_STEP_DATA.ID.eq(model.getId()))
                .returning()
                .fetchOptional()
                .orElseThrow(() -> new DataAccessException("Error updating entity: " + model.getId()))
                .into(MdmStepDataModel.class);
    }

    @Override
    public Boolean delete(Long id) {
        return dsl.deleteFrom(MdmStepData.MDM_STEP_DATA)
                .where(MdmStepData.MDM_STEP_DATA.ID.eq(id))
                .execute() == 1;
    }

}
