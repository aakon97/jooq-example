package ru.akon.jooqexample.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.akon.jooqexample.model.MdmServiceStepDataModel;
import ru.akon.jooqexample.model.MdmStepDataModel;
import ru.akon.jooqexample.repository.JooqRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class JooqServiceImpl implements JooqService {

    private final JooqRepository jooqRepository;

    @Override
    @SneakyThrows
    public String select() {
        // тупая выборка
        List<MdmStepDataModel> list = jooqRepository.select(164124174L);
        List<MdmServiceStepDataModel> list1 = jooqRepository.selectService(164124174L);
        // выборка с join
        List<MdmStepDataModel> list2 = jooqRepository.join(164124174L);
        List<MdmStepDataModel> list3 = jooqRepository.join1(164124174L);

        // insert
        MdmStepDataModel model = new MdmStepDataModel();
        model.setId(1L);
        model.setStatus("1234");

        model = jooqRepository.save(model);
        // update
        model.setRegionId(1);
        model = jooqRepository.update(model);
        // delete
        boolean deleted = jooqRepository.delete(model.getId());
        return list.size() + "";
    }
}
